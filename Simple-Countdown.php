<?php


/*
Plugin Name: Simple Count
down
Plugin URI: http://richard.perry-online.me.uk/technology/wordpress/simple-coun
tdown/
Author: r
ichardp2
Author URI: http://richard.perry-onl
ine.me.uk/
Description: A modified and simplified version of <a href="http://wp.dmonnier.com/">dmonnier</a>'s <a href="http://wordpress.org/extend/plugins/cssable-countdown/">CSSable Co
untdown</a>.


Version: 1.0
*/
define( 'CURRENT_VERSION', '1.0' );

/*
Based on Keith Wood's "Countdown for jQuery" v1.6.3 (http://keith-wood.name/countdown.html).

Following lines added to the original kw-jquery.countdown-1.6.3.js:

approx 102:
	// Class name for the period label marker.
	_amountLabelClass: 'countdown_label',
	
search for var showFull() approx 527 add before final </span>:
	'<span class="' + plugin._amountLabelClass + '">' +
	(labelsNum ? labelsNum[period] : labels[period]) + '</span>'
*/

require_once( 'includes/functions/build_countdown_args.php' );
require_once( 'includes/functions/print_countdown.php' );
require_once( 'includes/functions/list_of_timezones.php' );
require_once( 'includes/functions/convert_timezone_to_decimal.php' );
require_once( 'includes/functions/generate_layout.php' );
require_once( 'includes/functions/convert_wp_lang_to_valid_language_code.php' );
require_once( 'includes/shortcode.php' );

$list_of_instanced_ids = array();

$countdown_defaults = array(
	'title'		=> 'Countdown',
	
	'month'		=> date( 'm' ),
	'day'		=> date( 'd' ),
	'year'		=> date( 'Y' ),
	'hour'		=> 0,
	'minutes'	=> 0,
	'seconds'	=> 0,
	'timezone'	=> get_option( 'gmt_offset' ),
	
	// final format to be used in the countdown
	'format'		=> 'dhms',
	
	// internal formatting options
	'format_days'		=> 'd',
	'format_hours'		=> 'h',
	'format_minutes'	=> 'm',
	'format_seconds'	=> 's',
	
	// localization
	'lang'			=> get_bloginfo('language'),
);

$periods = array(	'format_days'		=> 'd',
					'format_hours'		=> 'h',
					'format_minutes'	=> 'm',
					'format_seconds'	=> 's');

class Simple_Countdown_Widget extends WP_Widget
{
    ////////////////////////////////////////////////////////////////
	//	constructor
	////////////////////////////////////////////////////////////////
    function __construct()
	{		
		parent::__construct( 'simple_countdown', __( 'Simple Countdown' ), array(
			'classname'   => 'widget_simple_countdown',
			'description' => __( 'A jQuery countdown widget based on Keith Wood\' Countdown for jQuery' ),
		) );
		// initialize default values
		global $countdown_defaults;
		$this->defaults = $countdown_defaults;
		
		// verify activation/upgrade option
		/*
			http://make.wordpress.org/core/2010/10/27/plugin-activation-hooks-no-longer-fire-for-updates/
			
			The proper way to handle an upgrade path is to only run an
			upgrade procedure when you need to. Ideally, you would store a
			"version" in your plugin's database option, and then a version
			in the code. If they do not match, you would fire your upgrade
			procedure, and then set the database option to equal the version
			in the code.
		*/
		$db_version = get_option( 'simple-countdown_version' );
		
		switch ( $db_version )
		{
			// plugin is up-to-date
			case CURRENT_VERSION: break;
			
			// db_version doesn't exist (plugin activation)
			case FALSE:	add_option( 'simple-countdown_version' , CURRENT_VERSION );
				break;
				
			// db_version is old (upgrade)
			default:	update_option( 'simple-countdown_version', CURRENT_VERSION );
				break;
		}

		// initialize jQuery
		wp_enqueue_script( 'jquery' );
		
		// initialize javascript
		wp_enqueue_script( 'kw-countdown', plugins_url( 'js/kw-jquery.countdown-1.6.3.js', __FILE__ ), 'jquery', '1.0' );
		
		// initialize localization files
		$kbw_lang = convert_wp_lang_to_valid_language_code( $this->defaults['lang'] );
		
		if ( !empty( $kbw_lang ) )
		{
			wp_enqueue_script( 'countdown-l10n',
									plugins_url( 'langs/jquery.countdown-' . $kbw_lang . '.js', __FILE__ ),
									'simple-countdown',
									'1.0',
									false);
		}
		
		// initalize frontend CSS
		wp_enqueue_style( 'simple-countdown-style-default', plugins_url( 'includes/css/countdown-default.css', __FILE__ ), '', '1.1' );
		
		// initialize admin CSS and JS
		function my_enqueue($hook)
		{
			if ( 'widgets.php' != $hook )
			{
				return;
			}
			
			wp_enqueue_style( 'simple-countdown-admin-style', plugins_url( 'includes/css/countdown-admin.css', __FILE__ ), '', '1.1' );
			wp_enqueue_script( 'simple-countdown_admin-toggle', plugins_url( 'js/admin-toggle.js', __FILE__ ), 'jquery', '1.0' );
		}
		add_action( 'admin_enqueue_scripts', 'my_enqueue' );
    } // end constructor
	
	////////////////////////////////////////////////////////////////
	//	outputs the HTML of the widget (front end)
	////////////////////////////////////////////////////////////////
    function widget($args, $instance) {
		extract( $args );

		$widget_options = wp_parse_args( $instance, $this->defaults );
		
		$title = apply_filters( 'widget_title', $instance['title'] );

		echo $before_widget . "\n\n";
		
		echo ( !empty( $title ) ?  $before_title . $title . $after_title : '' ) . "\n\n";
		
		include( 'includes/widget.php' );
		
		echo "\n" . $after_widget . "\n";
		
    } // end widget

	////////////////////////////////////////////////////////////////
	//	processes widget options to be saved
	////////////////////////////////////////////////////////////////
    function update($new_instance, $old_instance) {
		$new_instance['title'] = strip_tags( $new_instance['title'] );
		
		// sanity check: month/day/year
		if ( $new_instance['month'] < 1 )	{ $new_instance['month'] = 1; }
		if ( $new_instance['month'] > 12 )	{ $new_instance['month'] = 12; }
		if ( $new_instance['day'] < 1 )		{ $new_instance['day'] = 1; }
		
		// sanity check: basic formatting periods
		global $periods;
		$num_never = 0;
		
		foreach ( $periods as $pd => $val )
		{
			switch( $new_instance[$pd] )
			{
				case 'd':
				case 'D':
				case 'h':
				case 'H':
				case 'm':
				case 'M':
				case 's':
				case 'S':		break;
				case '-1':		$num_never++; break;
				default:		$new_instance[$pd] = strtoupper( $periods[$pd] );
			}
		}
		
		// sanity check: what if all formatting periods are "never show"?
		if ( $num_never == sizeof($periods) )
		{
			$new_instance['format_seconds'] = 'always';
		}
		
		// sanity check: compact and layouts
		if ( $new_instance['layout_type'] != 'compact' )
		{
			$new_instance['compact'] = false;
		}
		
		switch ( $new_instance['layout_type'] )
		{
					case 'list':		$new_instance['layout'] = generate_layout( $new_instance );
			break;	case 'text':		$new_instance['layout'] = '';
			break;	case 'compact':		$new_instance['compact'] = true;
			break;	default:			$new_instance['layout_type'] = 'default';
		}
		
        return $new_instance;
    } // end update

	////////////////////////////////////////////////////////////////
	//	outputs the HTML for the admin options (backend)
	////////////////////////////////////////////////////////////////
    function form($instance)
	{
		include( 'includes/form.php' );
	} // end form
}

////////////////////////////////////////////////////////////////
//	register the widget
//
//	11 Feb 2014:
//	Removed anonymous function because they require PHP 5.3+.
//	Resolves support thread "Plugin could not be activated: fatal error"
////////////////////////////////////////////////////////////////
function simple_countdown_register_widgets()
{
	register_widget( 'Simple_Countdown_Widget' );
}

add_action( 'widgets_init', 'simple_countdown_register_widgets' );