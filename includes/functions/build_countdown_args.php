<?php
function build_countdown_args( $unique, $widget_options )
{
	extract( $widget_options, EXTR_SKIP );
	
	$whichway = 'until';
	$quickdate = intval( $year ) . ', ' .
				( intval( $month ) - 1 ). ', ' .
				intval( $day ) . ', ' .
				$hour . ', ' .
				$minutes . ', ' .
				$seconds . ', 0';

	// create an array of all possible options
	$build_args = array(
		$whichway			=> 'new Date(' . $quickdate . ')',
		
		// formatting
		'format'			=> $format,
		
		// localization
		'lang'			=> convert_wp_lang_to_valid_language_code( $lang ),
	);
	
	// print_args: [key] => (bool) are quotes required?
	
	// these values are always required
	$print_args = array( $whichway	=> false,
						'format'	=> true);
	
	return array( $build_args, $print_args );
}
?>