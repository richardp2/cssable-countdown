<?php
function print_countdown( $unique, $build_args, $print_args ) {
	
?>
	<div id="<?php echo $unique; ?>" class="simple-countdown layout-type_default"></div>
			
	<script type="text/javascript"> 
	jQuery(document).ready(function($) {
		$('#<?php echo $unique; ?>').countdown({
<?php
		foreach ( $print_args as $req_arg => $do_quote)
		{
			// ignore empty expiryText
			if ( 'expiryText' == $req_arg && false === $build_args['hasExpiryText'] )
			{
				continue;
			}
			
			echo "\t\t\t" . $req_arg . ': ' . ( $do_quote ? "'" : '' ) . $build_args[$req_arg] . ( $do_quote ? "'" : '' ) . ',' . "\n";
		}
?>
		}); 
	});
	</script>
<?php
}
?>