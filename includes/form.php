<?php
$widget_options = wp_parse_args( $instance, $this->defaults );
extract( $widget_options, EXTR_SKIP );


global $periods;
?>

<p>
	<label for="<?php echo $this->get_field_id( 'title' ); ?>">
		<?php _e( 'Title:' ); ?>
	</label>
	<input id="<?php echo $this->get_field_id( 'title' ); ?>" 
			class="widefat" 
			name="<?php echo $this->get_field_name( 'title' ); ?>" 
			type="text" 
			value="<?php echo $title; ?>">
</p>
<p>
	<label for="<?php echo $this->get_field_id('month'); ?>">
		<?php _e('Date:'); ?>
	</label>
	
	<input id="<?php echo $this->get_field_id('month'); ?>"
			name="<?php echo $this->get_field_name('month'); ?>"
			type="text"
			value="<?php echo $month; ?>"
			size="2"
			placeholder="MM"
			maxlength="2" />
	
	/
	
	<input id="<?php echo $this->get_field_id('day'); ?>"
			name="<?php echo $this->get_field_name('day'); ?>"
			type="text"
			value="<?php echo $day; ?>"
			size="2"
			placeholder="DD"
			maxlength="2" />
			
	/
	
	<input id="<?php echo $this->get_field_id('year'); ?>"
			name="<?php echo $this->get_field_name('year'); ?>"
			type="text"
			value="<?php echo $year; ?>"
			size="4"
			placeholder="YYYY"
			maxlength="4" />
</p>

<p>
	<label for="<?php echo $this->get_field_id('hour'); ?>">
		<?php _e('Time:'); ?>
	</label>
	
	<input id="<?php echo $this->get_field_id('hour'); ?>"
			name="<?php echo $this->get_field_name('hour'); ?>"
			type="text"
			value="<?php echo $hour; ?>"
			size="2"
			placeholder="hh"
			maxlength="2" />
	
	:
	
	<input id="<?php echo $this->get_field_id('minutes'); ?>"
			name="<?php echo $this->get_field_name('minutes'); ?>"
			type="text"
			value="<?php echo $minutes; ?>"
			size="2"
			placeholder="mm"
			maxlength="2" />
			
	:
	
	<input id="<?php echo $this->get_field_id('seconds'); ?>"
			name="<?php echo $this->get_field_name('seconds'); ?>"
			type="text"
			value="<?php echo $seconds; ?>"
			size="2"
			placeholder="ss"
			maxlength="2" />
</p>